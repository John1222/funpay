<?php
namespace brocoder;

class TransactionConfirmationSMS
{
    private int $account;
    private float $amount;
    private int $password;
    
    public function __construct( string $textOfSms )
    {
        $__textOfSms = "{$textOfSms}\n";

        preg_match( '/\\d{13}(?!,)/', $__textOfSms, $matches );
        $this->account = $matches[ 0 ];

        preg_match( '/\\d*?[,\\\.].*?(?=\D)/', $__textOfSms, $matches );
        $this->amount = str_replace( ',', '.', $matches[ 0 ] );
        
        preg_match( '/(?<!\\d)(\\d{4})(?=\n|\r\n|\r|<br>)/', $__textOfSms, $matches );
        $this->password = $matches[ 0 ];
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getPassword(): int
    {
        return $this->password;
    }
}