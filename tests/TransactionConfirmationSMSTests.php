<?php
use brocoder\TransactionConfirmationSMS;
use PHPUnit\Framework\TestCase;

class TransactionConfirmationSMSTests extends TestCase
{
    private int $account;
    private float $amount;
    private int $password;
    
    function setUp(): void
    {
        $this->generateInputData();
    }

    /**
     * Поля могут меняться местами
     */
    function testResistanceToFieldsShuffle()
    {
        $textsOfSms = [
            "Пароль: {$this->password}\nСпишется {$this->amount}р.\nПеревод на счет: {$this->account}",
            "Перевод на счет: {$this->account}\nСпишется: {$this->amount}р.\nПароль: {$this->password}\n",
            "Перевод на счет: {$this->account}\nПароль: {$this->password}\nСпишется: {$this->amount}р.\n"
        ];
        foreach( $textsOfSms as $textOfSms ) {
            $sms = new TransactionConfirmationSMS( $textOfSms );
            $this->assertEquals( $this->account, $sms->getAccount() );
            $this->assertEquals( $this->amount, $sms->getAmount() );
            $this->assertEquals( $this->password, $sms->getPassword() );
        }
    }

    /**
     * Заголовки полей могут изменяться
     */
    function testResistanceToFieldsTitlesChange()
    {
        $sms = new TransactionConfirmationSMS( "Пороль: {$this->password}\nСпишеца: {$this->amount}р.\nПеревот прямо в рот: {$this->account}" );
        $this->assertEquals( $this->account, $sms->getAccount() );
        $this->assertEquals( $this->amount, $sms->getAmount() );
        $this->assertEquals( $this->password, $sms->getPassword() );
    }

    /**
     * Никто не знает, какие будут разделители тайтла поля и значения (если будут вообще)
     */
    function testResistanceToDelimiterChange()
    {
        $delimiters = [ ';', ':', ' ', '    ', '   :  ' ];
        foreach( $delimiters as $delimiter ) {
            $this->generateInputData();
            $sms = new TransactionConfirmationSMS( sprintf(
                "Спишется%s {$this->amount}р.\nПеревод на счет%s{$this->account}\nПароль%s{$this->password}",
                $delimiter, $delimiter, $delimiter
            ) );
            $this->assertEquals( $this->account, $sms->getAccount() );
            $this->assertEquals( $this->amount, $sms->getAmount() );
            $this->assertEquals( $this->password, $sms->getPassword() );
        }
    }

    /**
     * У суммы может меняться разделитель десятичной дроби и указываться валюта
     */
    function testResistanceToChangeAmountFormat()
    {
        $decimalSeparators = [ ',', '.' ];
        $currencies = [ 'р', '$', '€', '', 'р.', '$.', '€.', '.' ];
        foreach( $decimalSeparators as $decimalSeparator ) {
            foreach( $currencies as $currency ) {
                $sms = new TransactionConfirmationSMS(
                    "Пароль: {$this->password}\nСпишется: {$this->amount}{$decimalSeparator}25{$currency}\nПеревод на счет: {$this->account}"
                );
                $this->assertEquals( $this->account, $sms->getAccount() );
                $this->assertEquals( $this->amount, $sms->getAmount() );
                $this->assertEquals( $this->password, $sms->getPassword() );
            }
        }
    }

    /**
     * Что если какой-нибудь олигарх захочет перевести сумму с 12-ю нулями?
     */
    function testResistanceToLargeAmount()
    {
        $sms = new TransactionConfirmationSMS( "Пароль: {$this->password}\nСпишется 1000000000000,25р.\nПеревод на счет: {$this->account}\n" );
        $this->assertEquals( $this->account, $sms->getAccount() );
        $this->assertEquals( 1000000000000.25, $sms->getAmount() );
        $this->assertEquals( $this->password, $sms->getPassword() );
    }

    /**
     * Символы переноса строк могут изменяться
     */
    function testResistanceToLineBreaksChange()
    {
        $lineBreaks = [ "\r\n", "\n", "\r", '<br>' ];
        foreach( $lineBreaks as $lineBreak ) {
            $sms = new TransactionConfirmationSMS( sprintf(
                "Пароль: {$this->password}%sСпишется {$this->amount}р.%sПеревод на счет: {$this->account}%s",
                $lineBreak, $lineBreak, $lineBreak
            ) );
            $this->assertEquals( $this->account, $sms->getAccount() );
            $this->assertEquals( $this->amount, $sms->getAmount() );
            $this->assertEquals( $this->password, $sms->getPassword() );
        }
    }

    /**
     * Перенос в конце текста смс может быть, а может не быть
     */
    function testResistanceToUnpredictableEndingLineBreak()
    {
        $endingLineBreaks = [ "", "\n", "\r\n", "\r" ];
        foreach( $endingLineBreaks as $endingLineBreak ) {
            $sms = new TransactionConfirmationSMS( "Пароль: {$this->password}\nСпишется {$this->amount}р.\nПеревод на счет: {$this->account}{$endingLineBreak}" );
            $this->assertEquals( $this->account, $sms->getAccount() );
            $this->assertEquals( $this->amount, $sms->getAmount() );
            $this->assertEquals( $this->password, $sms->getPassword() );
        }
    }

    private function generateInputData()
    {
        $this->account = '41' . mt_rand( 10000000000, 99999999999 );
        $this->amount = mt_rand( 1, 50000000 ) + floatval( ( '0.' . mt_rand( 1, 100 ) ) );
        $this->password = mt_rand( 1000, 9999 );
    }
}